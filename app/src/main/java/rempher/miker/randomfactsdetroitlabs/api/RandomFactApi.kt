package rempher.miker.randomfactsdetroitlabs.api

import rempher.miker.randomfactsdetroitlabs.ApiManager
import rempher.miker.randomfactsdetroitlabs.model.Fact
import retrofit2.Call
import retrofit2.http.GET

interface RandomFactApi {
    @GET(ApiManager.URL + 1)
    fun getRandomFact(): Call<Fact>
}