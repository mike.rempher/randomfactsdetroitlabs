package rempher.miker.randomfactsdetroitlabs.ui.main

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import rempher.miker.randomfactsdetroitlabs.model.Fact

class MainViewModel: ViewModel() {

    var factText: ObservableField<String> = ObservableField()

    fun loadRandomFact(fact: Fact?) {
        factText.set(fact?.randomFact)
    }
}