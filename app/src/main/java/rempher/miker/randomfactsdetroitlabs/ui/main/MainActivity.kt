package rempher.miker.randomfactsdetroitlabs.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import rempher.miker.randomfactsdetroitlabs.ApiManager
import rempher.miker.randomfactsdetroitlabs.R
import rempher.miker.randomfactsdetroitlabs.model.Fact
import retrofit2.Call
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var mViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        randomFactBtn.setOnClickListener {
            getRandomFact()
        }
    }

    private fun getRandomFact() {
         ApiManager.instance.randomFactApi
            .getRandomFact()
            .enqueue(object : retrofit2.Callback<Fact> {
                override fun onResponse(call: Call<Fact>, response: Response<Fact>) {
                    Log.d("MainActivity", response.body().toString())
                    mViewModel.loadRandomFact(response.body())
                }

                override fun onFailure(call: Call<Fact>, t: Throwable) {
                    Log.d("MainActivity", call.toString())
                }
            })
    }
}
