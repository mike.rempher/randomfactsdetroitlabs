package rempher.miker.randomfactsdetroitlabs

import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import rempher.miker.randomfactsdetroitlabs.api.RandomFactApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiManager private constructor() {

    private var mRetrofit: Retrofit? = null

    val randomFactApi: RandomFactApi
        get() = mRetrofit!!.create<RandomFactApi>(RandomFactApi::class.java)

    init {
        if (mRetrofit == null) {
            mRetrofit = createRetroFitClient(createHttpClient())
        }
    }

    private fun createRetroFitClient(client: OkHttpClient): Retrofit {
        val builder = Retrofit.Builder()
        return builder.baseUrl(URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
    }

    private fun createHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
            .build()
    }

    companion object {
        val instance = ApiManager()
       const val URL = "http://api.icndb.com/jokes/random/"
    }
}