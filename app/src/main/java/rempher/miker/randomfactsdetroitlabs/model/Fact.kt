package rempher.miker.randomfactsdetroitlabs.model

import com.google.gson.annotations.SerializedName

class Fact {
    @SerializedName("joke")
    var randomFact: String = ""
}
